package com.taco.jdbcTemplate;

import com.taco.enity.Order;

public interface OrderRepository {

    Order save(Order order);
}
