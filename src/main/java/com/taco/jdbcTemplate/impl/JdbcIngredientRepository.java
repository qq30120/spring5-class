package com.taco.jdbcTemplate.impl;

import com.taco.enity.Ingredient;
import com.taco.jdbcTemplate.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class JdbcIngredientRepository implements IngredientRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcIngredientRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<Ingredient> findAll() {
        return jdbcTemplate.query("SELECT id, name, type FROM Ingredient",
                this::mapRowToIngredient);
    }

    // JDBC 一般形式
//    @Override
//    public Ingredient findOne(String id) {
//        return jdbcTemplate.queryForObject(
//                "SELECT id, name, type from Ingredient WHERE id=?",
//                this::mapRowToIngredient,id);
//    }

    // JDBC Lambda 顯式寫法
    @Override
    public Ingredient findOne(String id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, name, type FROM Ingredient WHERE id=?",
                new RowMapper<Ingredient>() {
                    @Override
                    public Ingredient mapRow(ResultSet resultSet, int i) throws SQLException {
                        return new Ingredient(
                                resultSet.getString("id"),
                                resultSet.getString("name"),
                                Ingredient.Type.valueOf(resultSet.getString("type")));
                    }
                }, id);
    }


    private Ingredient mapRowToIngredient(ResultSet resultSet, int rowNum) throws SQLException {
        return new Ingredient(
                resultSet.getString("id"),
                resultSet.getString("name"),
                Ingredient.Type.valueOf(resultSet.getString("type"))
        );
    }

    /**
     * 兩種方式存儲
     * 1. update
     * 2. SimpleJdbcInsert
     * @param ingredient
     * @return
     */
    @Override
    public Ingredient save(Ingredient ingredient) {
        jdbcTemplate.update(
                "INSERT INTO Ingredient (id, name, type) values (?,?,?)",
                ingredient.getId(),
                ingredient.getName(),
                ingredient.getType().toString());
        return ingredient;
    }
}
