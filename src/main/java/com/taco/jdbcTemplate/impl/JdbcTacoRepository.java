package com.taco.jdbcTemplate.impl;

import com.taco.enity.Ingredient;
import com.taco.enity.Taco;
import com.taco.jdbcTemplate.TacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

@Repository
public class JdbcTacoRepository implements TacoRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcTacoRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Taco save(Taco taco) {
        long tacoId = saveTacoInfo(taco);
        taco.setId(tacoId);
        for (Ingredient ingredient : taco.getIngredientList()) {
            saveIngredientToTaco(ingredient, tacoId);
        }

        return taco;
    }

    // 存入Taco 資料表 並拿出 Id
    private long saveTacoInfo(Taco taco) {
        taco.setCreatedAt(new Date());
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement("insert into Taco (name, createdAt) values (?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, taco.getName());
                ps.setDate(2, new java.sql.Date(System.currentTimeMillis()));
                return ps;
            }
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    // 存進Taco_Ingredients
    private void saveIngredientToTaco(Ingredient ingredient, long tacoId) {
        jdbcTemplate.update(
                "INSERT INTO Taco_Ingredients (taco , ingredient) values (? ,?)",
                tacoId, ingredient.getId());
    }
}
