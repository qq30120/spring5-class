package com.taco.jdbcTemplate;

import com.taco.enity.Taco;

public interface TacoRepository {

    Taco save(Taco taco);

}
