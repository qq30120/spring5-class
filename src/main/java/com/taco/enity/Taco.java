package com.taco.enity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 玉米餅
 */
@Data
@Entity
public class Taco {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date createdAt;

    // 名稱
    @NotNull
    @Size(min = 3,message = "名字長度需大於3")
    private String name;

    // 包含配料
    @Size(min = 1,message = "請至少選擇一項配料")
    @ManyToMany(targetEntity = Ingredient.class)
    private List<Ingredient> ingredientList = new ArrayList<>();

    @PrePersist
    void createdAt() {
        this.createdAt = new Date();
    }

}
