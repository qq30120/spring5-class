delete from Taco_Order_Tacos;
delete from Taco_Ingredients;
delete from Taco;
delete from Taco_Order;

delete from Ingredient;
insert into Ingredient (id, name, type)
values ('FLTO', '麵粉玉米餅', 'WRAP');
insert into Ingredient (id, name, type)
values ('COTO', '玉米餅', 'WRAP');
insert into Ingredient (id, name, type)
values ('GRBF', '碎牛肉', 'PROTEIN');
insert into Ingredient (id, name, type)
values ('CARN', '豬肉絲', 'PROTEIN');
insert into Ingredient (id, name, type)
values ('TMTO', '番茄丁', 'VEGGIES');
insert into Ingredient (id, name, type)
values ('LETC', '生菜', 'VEGGIES');
insert into Ingredient (id, name, type)
values ('CHED', '切達乳酪', 'CHEESE');
insert into Ingredient (id, name, type)
values ('JACK', '蒙特雷·傑克乳酪', 'CHEESE');
insert into Ingredient (id, name, type)
values ('SLSA', '莎莎醬', 'SAUCE');
insert into Ingredient (id, name, type)
values ('SRCR', '酸奶油', 'SAUCE');